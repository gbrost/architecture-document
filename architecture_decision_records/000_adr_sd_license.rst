ADR-00X: Self-Description License Attribute
===========================================

:adr-id: 00x
:revnumber: 1.0 
:revdate: 26-09-2021
:status: draft
:author: Catalogue Sub-WG
:stakeholder: Self-Description WP, Catalogue Sub-WG

Summary
-------

Self-Descriptions have both meta-data attributes about the Self-Description
itself and attributes for the entity that is described.

One optional meta-data attribute is "sd-license". The attribute defines the
license under which the issuer publishes the Self-Description.

Context
-------

There are multiple ways for Self-Descriptions to be published within the GAIA-X
eco-system. For example via the Registry, through the submission to a Catalogue 
or some inter-Catalogue synchronization mechanism. There are Services, like the
Catalogue which need to distribute complete Self-Descriptions or integrate parts
of it into a "combined work" (e.g. query results). If the Self-Description was
submitted through an other component, a permission for redistribution might be
missing.

To overcome this and to have a traceable and transparent documentation about
the granted rights, a license should be added for the Self-Description itself.
To not require additional tracking of licences, this information should be added
to the Self-Description, too.

It is not required that the Self-Descriptions is published under an Open Source 
license. Providers can issue Self-Descriptions under a restrictive licence, e.g. 
to keep it internally or for a particular Ecosystem.

Decision Statements
-------------------

Add meta-data attributes to the Self-Description, describing the Self-Description
itself.

Self-Descriptions can define the meta-data attribute "sd-license". The attribute
defines the license under which the issuer publishes the Self-Description. For
the definition of licences the SPDX License List [SPDX] should be used, which
covers all relevant open source licences. In this case the sd-license attribute 
must refer to an instance of the class "SPDX License List" (IRI: 
"http://spdx.org/rdf/terms#ListedLicense"). A list of supported licences can be
found in this GitHub repository [SPDX JSON-LD]. Proprietary licences can be
specified, by a plain string literal value. In this case automated processing
might not be possible and Catalogues might reject the acceptance of such
Self-Descriptions. Unknown or incompatible licences might be ignored by Catalogues.
This is not considered discriminatory behavior, as long as this policy is stated
publicly and applied uniformly to all Self-Descriptions.


For public Self-Descriptions (not private/confined to Ecosystems) it is
recommended to use the Creative Commons Attribution 4.0 International license
(SPDX-identifier "CC-BY-4.0", IRI "http://spdx.org/licenses/CC-BY-4.0").

A Catalogue implementation might ignore metadata-attributes in the results
of queries.

Consequences
------------

The issuer of a Self-Description can decide under which license the Self-Description
should be published.

By evaluating the meta-data attribute "sd-license", the Participants -- and in
particular the Catalogues -- are protected from mistakenly distributing
copyrighted material.

ADR References
--------------

- ADR-001: JSON-LD as the Exchange Format for Self-Descriptions

External References
-------------------

* [SPDX] https://spdx.org/licenses
* [SPDX JSON-LD] https://github.com/spdx/license-list-data/tree/master/jsonld