ADR-###: Interconnection Service
==============================================

:adr-id: 000
:revnumber: 1.0
:revdate: 09-10-2020
:status: proposed
:author: Alina Rubina
:stakeholder: WP Interconnection and Networking

Summary
-------
In the Technical Architecture Paper (published on 04.06.20) the definition of Interconnection Service is missing.
The graphical representation is found in Figure 9 in [1]. The aim of this ADR is to define this service and put a base for the definition of interconnection service attributes.
Example of such can be found in [3].


Context
-------
These kind of services can be booked by IX-API [2], that was proposed in 
https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/blob/adr_interconnection/architecture_decision_records/draft_interc_platform.rst.

Decision Statements
-------------------
An Interconnection Service represents a service to create and configure an interconnection between two or multiple nodes  [^1].
In addition to the actual connection between the nodes, the service can allow to configure additional settings, e.g., the available bandwidth or security related settings.
The Interconnection Service can be booked and configured through an interface or automatically, and contributes to the interoperability on the infrastructure level by interconnecting the different nodes with each other.

[^1]: Interconnection Services can comprise but are not limited to services offered by Internet Service Providers, Internet Exchange Points, Interconnection Exchanges, or Cloud Service Providers as Network as a Service (NaaS) (cf. ISO/IEC 17788 and ITU-T Y.3500).


Consequences
------------
The definition of this service should become a part of Architecture Paper.

ADR References
--------------

* ADR-000 - Template ADR

External References
-------------------

[1] https://www.data-infrastructure.eu/GAIAX/Redaktion/EN/Publications/gaia-x-technical-architecture.pdf?__blob=publicationFile&v=5
[2] https://ix-api.net/
[3] https://docs.ix-api.net/v2/redoc#operation/network-service-configs_list
