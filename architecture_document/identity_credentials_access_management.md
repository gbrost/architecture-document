# Identity, Credentials & Access Management

## Identity and Access Management

Identities, which are used to gain access to the Ecosystem, rely on unique Identifiers and a list of dependent attributes. Gaia-X uses existing Identities and does not maintain them directly. Uniqueness is ensured by a specific Identifier format relying on properties of existing protocols. The Identifiers are comparable in the raw form and should not contain more information than necessary (including Personal Identifiable Information). Trust - confidence in the Identity and capabilities of Participants or Resources - is established by cryptographically verifying Identities using the Gaia-X Compliance Service, which guarantees proof of identity of the involved Participants to make sure that Gaia-X Participants are who they claim to be. In the context of Identity and Access Management, the digital representation of a natural person, acting on behalf of a Participant, is referred to as a Principal. As Participants need to trust other Participants and Service Offerings provided, it is important that the Gaia-X Trust Framework provides transparency for everyone. Therefore, proper lifecycle management is required, covering Identity onboarding, maintaining, and offboarding within Ecosystem. The table below shows the Participant Lifecycle Process.

| Lifecycle Activity | Description |
|--------------------|-------------|
| Onboarding         | The Gaia-X Compliance Service and optional individual ecosystems workflow validates and signs the Self-Description provided by a Visitor (the future Participant/Principal). |
| Maintaining        | Trust related changes to the Self-Descriptions are recorded in a new version and validated and signed by the same entities involved during the Onboarding. This includes information controlled by the Participant/Principal. |
| Offboarding        | The offboarding process of a Participant is time-constrained and involves all dependent Participants/Principals. This includes keypair revocation by the entities involved during the Onboarding. |

### Participant Lifecycle Process

An Identity is composed of a unique Identifier and an attribute or set of attributes that uniquely describe an entity within a given context. The lifetime of an Identifier is permanent. It may be used as a reference to an entity well beyond the lifetime of the entity it identifies or of any naming authority involved in the assignment of its name. Reuse of an Identifier for different entity is forbidden. Attributes will be derived from existing identities as shown in the IAM Framework Document v1.2[^17].

[^17]: See the IAM Framwork version 1.2 for details: https://community.gaia-x.eu/s/P23ZJNLyjf7n7Zp?path=%2FReleases.

A 'Secure Digital Identity' is a unique Identity with additional data for robustly trustworthy authentication of the entity (i.e. with appropriate measures to prevent impersonation). This implies that Gaia-X Participants can self-issue Identifiers for such Identities. It is solely the responsibility of a Participant to determine the conditions under which the Identifier will be issued.  Identifiers shall be derived from the native identifiers of an Identity System without any separate attribute needed. The Identifier shall provide a clear reference to the Identity System technology used. Additionally, the process of identifying an Identity Holder is transparent. It must also be possible to revoke issued Identity attributes[^18].

[^18]: For more details on Secure Identities, see Plattform Industrie 4.0: Working Group on the Security of Networked Systems. (2016). Technical Overview: Secure Identities. <https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/secure-identities.pdf> as well as Chapter 3.4 in the IAM Framework v1.2: https://community.gaia-x.eu/s/P23ZJNLyjf7n7Zp?path=%2FReleases.

### Layered Identity and Access Management

The Identity and Access Management relies on a two-tiered approach. In practice, this means that Participants use a selected few Identity Systems for mutual dentification and trust establishment, SSI being the recommended option for interoperability. After trust is established, underlying existing technologies already in use by articipants (on the "Principal layer") can be federated and reused, for example Open ID Connect or domain specific x509-based communication protocols.

Gaia-X Participants might need to comply with additional requirements on the type and usage of credentials management applications such as mandatory minimum-security requirements, such as Multi-factor authentication. Server-to-Server Communication plays a crucial role in Gaia-X.

This chapter describes the components required to provide an attested secure chain of trust & identities. Service implementations and the corresponding service delivery layer may include End-User services, distributed microservice architectures across multiple Participant domains, and/or cross domain data or digital service delivery

#### Architecture principles for this approach

Mutual trust based on mutually verifiable Participant identities between contracting parties, Provider and Consumer, is fundamental to federating trust and identities in the Principal layer. Heterogeneous ecosystems across multiple identity networks in the Participant layer must be supported as well as heterogeneous environments implementing multiple identity system standards. The high degree of standardization of Participant layer and Principal layer building blocks of the Gaia-X Trust Framework must ensure that there is no lock-in to any implementation of identity network and Identity System likewise.

![Federated Trust](media/federated_trust_hld.png)

#### Chain of trust and identity

The Gaia-X Participant Identity & Trust framework delivers a secure chain of trust and identities to the service delivery layer.

**Mutual participant verification**

In the Participant layer, the Gaia-X Trust Framework specifies how to resolve and verify the Participant identity of the contracting parties. The Consumer verifies the Provider identity, the Provider verifies the Consumer identity.
Successful mutual Participant verification results in a verified Participant Token representing the trust between Provider and Consumer.

**Identity System federation**

In the Principal layer, the Federation Plugin implements the functionality to federate trust between the Identity Systems of the contracting Participants based on the successful mutual Participant verification described above.  
The federation of trust between the identity systems is based on the identity system standard implemented for the service delivery layer. Required for the federation is a secure mutual exchange of the required federation metadata. This exchange must be secured based on the Verified Participant Token.
Exemplary Identity Systems standards supporting federation are: OIDC/OAuth2 (draft), SAML, SPIFFE/SPIRE. Identity System federation may also include federating the trust between certificate authorities supporting X.509 for Principals.

**Identification and Authentication**

Once successfully federated, the Identity Systems are enabled to identify and authenticate the Principals in the service delivery layer of the contracting parties. Federated Principal identities are mutually trusted based on the federation of the Identity Systems of the contracting Participants.

**Principal Identity Integration Layer**

While the interface to the Gaia-X Federated Trust Component is standardized, the federation mechanism of the Federation Plugin is specific to the implemented Identity System supporting current and future standards like OIDC/OAuth2 (draft), SPIFFE/SPIRE, PKI.
Furthermore, multiple Identity Systems required for complex service offerings, like for example OIDC for user Principals, SPIRE for service Principals, are perfectly supported meaning that multiple Identity Systems on either side can be federated by corresponding plugins based on the very same mutual Participant identity verification if required for the service delivery.

## Gaia-X Federation Servies for Notarization and Credential storage

Gaia-X AISBL defines a Trust Framework that manifests itself in the form of two services:

- the Gaia-X Registry, detailed in the Operating model chapter
- the Gaia-X Compliance service, as the service implementing the set of rules described in the Gaia-X Trust Framework document.

The Notarization Service will supprt the issuance of Verficiable Credentials to the Self Descriptions which can be stored in the Personal- or Organizational Credential Manager.