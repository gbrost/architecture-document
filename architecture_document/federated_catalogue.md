# Federated Catalogue

The goal of Catalogues is to enable Consumers to find best-matching offerings and to monitor for relevant changes of the offerings. The Providers decide in a self-sovereign manner which information they want to make public in a Catalogue and which information they only want to share privately. A provider will usualy use an own Credential Store which keeps the Gaia-X Self-Descriptions together with their cryptographic key and publish offerings to multiple catalogues.

![Gaia-X Federated Catalogue](figures/Gaia-X_Federated_Catalogue.png)

A Provider registers Self-Descriptions with their universally resolvable Identifiers in the desired Catalogue to make them public relative to the Catalogue scope.
The Catalogue builds an internal representation of a knowledge graph out of the linked data from the registered and accessible seld-descriptions to provide interfaces to query, search and filter services offerings.

The system of Federated Catalogues includes an initial stateless Self-Description browser provided by the Gaia-X Association. In addition, ecosystem-specific Catalogues (e.g., for the healthcare
domain) and even company-internal Catalogues (with private Self-Descriptions to be used only internally) can be linked to the system of federated Catalogues. The Catalogue federation is used to exchange relevant Self-Descriptions and updates thereof. It is not used to execute queries in a distributed fashion.

Cross-referencing is enabled by unique Identifiers as described in [Identity and Access Management](federation_service.md#identity-and-access-management). While uniqueness means that Identifiers do not refer to more than one entity, there can be several Identifiers referring to the same entity. A Catalogue should not use multiple Identifiers for the same entity.

Gaia-X develops an extensible hierarchy of Schemas that define the terms used in Self-Descriptions and which must be supported by any Catalogue. It is possible to create additional Schemas specific to an application domain, an ecosystem, Participants in it, or Resources offered by these Participants.

A Schema may define terms (classes, their attributes, and their relationships to other classes) in an ontology. If it does, it must also define shapes to validate instances of the ontology against.

Self-Descriptions in a Catalogue are either loaded directly into a Catalogue or exchanged from
another Catalogue through inter-Catalogue synchronization functions.

Since Self-Descriptions are protected by cryptographic signatures, they
are immutable and cannot be changed once published. This implies that after any changes to a Self-Description, the Participant as the Self-Description issuer has to sign the Self-Description again and release it as a new version.
The lifecycle state of a Self-Description is described in the Self-Description Definition chapter.

## Protocol to Pull/Push information between Credential Store and Catalogue

This section needs to be written or it needs to reference a Technical Specification for Catalogues

## Synchronization of Catalogues

Including the synchronization with the Self-Description Lifecycle

This section needs to be written or it needs to reference a Technical Specification for Catalogues
