# Gaia-X Conceptual Model

The Gaia-X Conceptual Model, shown in the figure below, describes all concepts in the scope of Gaia-X and relations among them. Supplementary,
more detailed models may be created in the future to specify further aspects. Minimum versions of important core concepts in the form of mandatory attributes for Self-Descriptions are specified in the [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/). The general interaction pattern is further described in the section [Basic Interactions of Participants](usecase.md#basic-interactions-of-participants).

The Gaia-X core concepts are represented in classes. An entity highlighted in blue shows that an element is part of Gaia-X and therefore described by a Gaia-X Self-Description. The upper part of the model shows different actors of Gaia-X, while the lower part shows elements of commercial trade and the relationship to actors outside Gaia-X.

![Conceptual Model](figures/GAIA-X_models-Main.png)
*Gaia-X conceptual model*

## Participants

A Participant is an entity, as defined in ISO/IEC 24760-1 as "item
relevant for the purpose of operation of a [domain](federation_service.md#identity-and-trust) that has
recognizably distinct existence"[^7], which is onboarded and has a
Gaia-X Self-Description. A Participant can take on one or more of
the following roles: Provider, Consumer, Federator. Section [Federation Services](conceptual_model.md#federation-services)
demonstrates use cases that illustrate how these roles could be filled.
Provider and Consumer present the core roles that are in a
business-to-business relationship while the Federator enables their
interaction.

[^7]: ISO/IEC. IT Security and Privacy — A framework for identity management: Part 1: Terminology and concepts (24760-1:2019(en)). ISO/IEC. <https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en>

### Provider

A Provider is a Participant who operates Resources in the
Gaia-X Ecosystem and offers them as services. For any such service, the Provider defines the Service Offering including terms and
conditions as well as technical Policies. Furthermore, it provides the
Service Instance that includes a Self-Description and associated
Policies.

### Federator

Federators are in charge of the Federation Services and the Federation,
which are independent of each other. Federators are Gaia-X Participants.
There can be one or more Federators per type of Federation Service.

A Federation refers to a loose set of interacting actors that directly
or indirectly consume, produce, or provide related Resources.

### Consumer

A Consumer is a Participant who searches Service Offerings and consumes
Service Instances in the Gaia-X Ecosystem to enable digital offerings
for End-Users.

## Service Composition Model

### Assumptions

The generic Gaia-X service composition model is derived assuming availability of key related functions and systems within the Gaia-X Ecosystem. The first assumption is the availability of a Catalogue containing service offerings compliant with Gaia-X. These correspond to provider services with Self-Descriptions and credentials verified by the Gaia-X Compliance Service. In addition to these services, there can be external and independent services offered by multiple third parties. A requirement on these third-party service offerings is to provide independent services compatible with Gaia-X compliant services. It is thus possible to combine them to produce composite services. Means to check for compatibility and composability are provided and potentially built into the respective Self-descriptions. Service composition consequently assumes that searching for compatible and interoperable services in multiple Catalogues is possible. Whenever such capability is not available, service offerings nevertheless come along with service templates and services descriptors and descriptions that readily embed this key information. Hence, Self-descriptions have to contain key characteristics and must have the appropriate structure. They would embed the following TOSCA-like or similar descriptors:

- Capabilities
- Requirements
- Properties
- Operations
- Artifacts
- Compatibility, interoperability, composability, substitutability attributes (information, list, possibly reveal if service components are bundles)
- Interfaces

Furthermore, we assume embedded additional information in self-descriptions in the Catalogues. That is, providing information on how to chain services to ensure successful and failure free composition as well as guaranteed operation at instantiation and run time.

### Generic Service Composition Model

Considering an open European and international context involving multiple stakeholders and participants, the Gaia-X service composition model has to be abstract at the start of any initial service composition. The focus of the composition model is consequently on the service functional behavior, with no constraints, localization, preset values of non-functional attributes. Values are set only once the end users, tenants or consumers, have expressed their requirements, constraints and preferences. These values are gradually set as service composition moves from initial provisioning to the life cycle management of services at run time. Figure 1 depicts a high-level class diagram view of the service composition model.

![Service Composition Model](figures/Fig1_Abstract-High-level_Generic-SC-Model.png)
*Figure 1. Simplified and abstract conceptual service composition model*

This process shall by no means prevent or hamper interoperability, portability, substitution of services and applications. This should require no or minor changes. Figure 1 depicts the very abstract and simplified service composition model proposed in a previous release of the Gaia-X Architecture Document. Beyond this simplified and abstract service composition view and class diagram, Figure 2 illustrates a more elaborate class diagram representation of the service composition model. The figure includes details and related functions and modules involved in service composition starting from a user service request to the instantiation of the services by providers. It also includes and extends the Gaia-X conceptual model with an expanded view of the service composition model.

### Conceptual Service Composition Model

We start with a generic and abstract view of the conceptual service composition model and illustrate where it fits in the Gaia-X conceptual model. As far as the Gaia-X service composition model is concerned, both API and template-based services have to be included in the modelling framework.

![Service Composition](figures/Fig2_service_composition_-diagram-October-11-2022-Arch_Conceptual_Model_main.png)
*Figure 2. Gaia-X Conceptual Architecture Model with Service Composition focus*

Figure 3 depicts the need for a service discovery service that can search, match and select from a Catalogue. The search may concern other Catalogues and offers depending on the will of the users to combine service offerings from different sources with verified interoperability and compatibility characteristics available in the Catalogue itself. End users or tenants express this initial request and set both their requirements and constraints in their demand.

![Detailed Service Composition Model](figures/Fig3_service_composition_detailed-model-revisited-October-12-2022.png)
*Figure 3. Generic and abstract conceptual service composition model*

The service composition module first “analyzes, parses and decomposes” the service request. It then prepares a set of service discovery queries to find and select the candidate services among the service offerings via Gaia-X Federation Services. Once the user has negotiated and approved the selected services and providers, candidate services are returned to service composition in order to build service deployment and instantiation workflows and plans. A contract is also established accordingly. The service composition module realizes the functional version of the solution and initiates service binding with the providers’ provisioned resources. This abstract implementation plan is the final document of the Gaia-X Services Composition process, from which real implementations by suppliers will be developed. The actual deployment, binding and connectivity occur via an orchestrator, responsible for the coordination and organization of service instances, as well as the initial deployment, configuration and activation of such created service instances. The orchestrator interacts with deployment and configuration tools ensuring the actual configuration, control and dynamic adaptation of service instances. A service life cycle manager is also associated with the orchestrator to manage services at run time. The orchestrator and service life cycle manager are typically external and act as the interface between service composition plans and providers. Optionally, the consumer acting as broker can realize service composition. In this case, the consumer actively drives the orchestrators and partakes in the orchestration and service life cycle management according to compatible service deployment and management tools specified by the providers. These tools and management systems can be proprietary or readily available in the Catalogue and service offerings. Popular configuration and deployment tools and systems such as Terraform, Ansible, Heat and Salt Stack are examples used for deployment and configuration purposes in cloud infrastructures. If service instances and resources are hosted in containers rather than in virtual machines, an intermediate container orchestration and management system such as Kubernetes is then used in an intermediate step. These intermediate steps are out of the scope of this document and the Gaia-X service composition mode and ecosystem.

### Service composition steps

To pursue the description of the service composition model beyond what the previous section presented, we focus on the steps required to implement service composition and describe the functions required to achieve end-to-end service composition across multiple service providers. Figure 3 depicts these steps and starts with a service request from the user (end user, tenant, information system manager) request.

![Service Composition Activity Diagram](figures/Fig4_service_composition_-steps-activation-diagram-October-11-2022-Activation_diagram_-service_composition-steps.png)
*Figure 4. Gaia-X Compliant Service Composition Steps*

The user first describes the desired service using a domain specific description language, generally json and Yaml-based. This request is then parsed for specific end user requirements extraction and added (concatenated) to a generic and high-level catalogue that contains service descriptions as part of a Catalogue and holds only functional descriptions of services and applications relevant for the user request, to ensure interoperability, compatibility and portability requirements. These services are not localized a priori; location is to be set at a later stage in the service composition process. The same holds for some additional service attribute values, in some parts of the service description. These values are empty fields or wild cards that will only take on a specific values once the user service request has been parsed for preferences, constraints and specific limitations (such as provider type, restrictions on localization and geographical area, unconstrained naming and addressing spaces and more). Some attribute values are only set (or resolved) and known at later stages of the service composition (as late as steps 4 and 5).

The user-expressed constraints, at step 1, are added to the functional description at step 2 to produce the actual service request that will be used to derive in a third step an extended representation of the service request. This extended representation, usually in the form of a tree (or complex service graph) representation and description of the extended service, now contains required services and resources as well as generic hosting nodes (not yet fully specified) needed to fulfill the service request. All properties and requirements from the initial service request therefore propagate to these tree nodes and leafs. This tree contains the relationships and dependencies of all services involved in building a solution to the initial user request and have as mentioned inherited all the service properties, constraints, restrictions and obligations stated by the user in step 1. The fourth step is service discovery that will search in multiple catalogues.

The output of the fourth step produces, with the help of orchestrators and service composition, work flows and deployment plan production engines (external or even taken from the Gaia-X service offerings) to deploy the service instances and ensure their interconnection. The result is the production of a composite service graph that fulfills the initial end user request and its concrete instantiation in multiple hosting infrastructures and providers. In Figure 3, the service life cycle manager handles the service management at run time by interacting with the involved providers. This manager makes recommendations about possible adaptations, substitutions and adjustments of services depending on operational conditions to maintain SLAs according to established contracts between all stakeholders and processes. These contracts are taken into account during stages 1 to 5 of the service composition process depicted in Figure 4. These steps, not shown at this stage, will receive special attention in a future dedicated document.

A set of annexes provide additional insight and lower level details on the Gaia-X service composition model. The first annex describes a practical use case that current cloud services offerings can fulfill and provide. The other annexes report class diagrams and models of services and resources involved at the storage (data services), compute and infrastructure levels.

Annex 1, found at the end of this document, provides a practical "service composition model example" involving multiple cloud providers, services and  infrastructures that can be accomplished using currently available cloud services.

## Resources

Resources describe in general the goods and objects of the Gaia-X Ecosystem.

A `Service Offering` can be associated with other `Service Offerings`.

```mermaid
classDiagram
    ServiceOffering o-- Resource: aggregation of
    Resource o-- Resource: aggregation of

class Resource{
    <<abstract>>
}

    Resource <|-- "subclass of" PhysicalResource: 
    Resource <|-- "subclass of" VirtualResource
    VirtualResource <|-- "subclass of" InstantiatedVirtualResource

    InstantiatedVirtualResource <..> ServiceInstance : equivalent to
```

*Resource Categories*

A Resource can be a:

- Physical Resource: it has a weight, position in space and represents physical entity that hosts, manipulates, or interacts with other physical entities.
- Virtual Resource: it represents static data in any form and necessary information such as dataset, configuration file, license, keypair, an AI model, neural network weights, …
- Instantiated Virtual Resource: it represents an instance of a Virtual Resource. It is equivalent to a Service Instance and is characterized by endpoints and access rights.

## Interconnection Point Identifiers

To allow multiple data sources and infrastructures to connect, Gaia-X introduces the concept of the **Interconnection Point Identifier (IPI)**. An IPI is a specific [Service Access Point](https://en.wikipedia.org/wiki/Service_Access_Point) that identifies where resources can interconnect. It needs to be:

- resolvable within the GAIA-X Conceptual model
- hierachical
- support multiple OSI-Layers and technologies (both physical and virtual)
- be globally unique

An IPI would either point to a physical resource or reference an instantiated virtual resource. Each Service Offering in a GAIA-X federated Catalogue must have an IPI to identify on how to connect to the Service Offering.

To connect two or more service offerings a GAIA-X Consumer must use a Network Connectivity Service from a GAIA-X federated Catalogue. Those Network Connectivity Services follow the OSI-Model and need to have IPIs that need to be able to reference interconnection points for

- **Physical Connection Services** (Layer 1)
- **Link Connection Services** (Layer 2)
- **Network Connection Services** (Layer 3)
- and potential other, higher layer interconnection services, such as **Application Layer Connection Services** etc., i.e. when conneting to an URI

In addition, to follow the Trust Framework, each IPI has at least to reference:

- The **geographical location** of the IPI, although the designation of an "International" or "Public" Location is possible to allow "floating" IP Blocks or global Satellite Networks. The geographical location follows the [ISO 3166 country codes](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) or other GAIA-X wide applicable location labels and should mainly be used as a reference for the jurisdiction of a given IPI. The geographical location should always reference the jurisdiction in which the Network Connectivity Service originates and is ordered from (the "A-End") or the jurisdiction the service as a whole is governed under.
  
- The **Provider** of the IPI, which provides the IPI, but not necessary *owns* it. For example, the *provider* of an IP Number is the [IANA](https://iana.org/), but the owner can be the GAIA-X user that has subscribed this IP from his Network Provider. The provider referenced can be a validated GAIA-X Provider, so that his services can be found in the respective federated catalogue, but can also be a provider outside of the GAIA-X Ecosystem to allow for "outside ressource" references. A Network Connectivity Service offered by a non-GAIA-X registered provider is always considered an untrusted connection.

As an IPI can specify the Interconnection point of a **Network Connectivity Service Offering** or an **Instantiated Network Connectivity Ressource** it needs to have additional **Type or Provider Specific Parameters** to be able to describe from where to where a given Network Connectivity Service Offering is connecting to and from (the potential *A- and Z-Ends*). Those A- and Z-End IPIs can be less specific than the designations of the instantiated Network Connectivity Ressources. As an example there can be a Network Connectivity Service Offering that offers connectivity to the AWS Direct Connect Product, thus offering an interconnection to AWS Services, while the instantiated ressource also references the actual AWS Tenant or VPC that is connected. The later parameters are part of the **Instantiation Requirements**.

![Conceputal Model with IPI](figures/GAIA-X_models-main_IPI.png.png)
*Gaia-X conceptual model with IPI*

## Policies

Policy is defined as a statement of objectives, rules, practices, or regulations
governing the activities of Participants within Gaia-X.
From a technical perspective Policies are statements, rules or
assertions that specify the correct or expected behaviour of an
entity[^9][^10].

The Policy Rules and Labelling Document, previously [Policy Rules Document](https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X_Policy-Rules_Document_v22.04_Final.pdf) explains the general Policies defined by the Gaia-X Association for all
Providers and Service Offerings. They cover, for example, privacy or cybersecurity policies
and are expressed in the conceptual model indirectly via Gaia-X Federation Service Compliance
and as attributes of the Resources, Service Offerings, and Service Instances.

These general Policies form the basis for detailed Policies for a particular Service Offering, which can be defined additionally and contain particular restrictions and obligations defined by the respective Provider or Consumer. They occur either as a Provider Policy (alias Usage Policies) or as a Consumer Policy (alias Search Policy):

- A Provider Policy/Usage Policy constrains the Consumer's use of a Resource. *For example, a Usage Policy for data can constrain the use of the data by allowing to use it only for x times or for y days.*

- A Consumer Policy describes a Consumer's restrictions of a requested Resource. *For example, a Consumer gives the restriction that a Provider of a certain service has to fulfil demands such as being located in a particular jurisdiction or fulfil a certain service level.*

In the Conceptual Model, they appear as attributes in all elements related to Resources.
The specific Policies have to be in line with the general Policies in the Policy Rules and Labelling Document.

[^9]: Singhal, A., Winograd, T., & Scarfone, K. A. (2007). Guide to secure web services: Guide to Secure Web Services - Recommendations of the National Institute of Standards and Technology. Gaithersburg, MD. NIST. <https://csrc.nist.gov/publications/detail/sp/800-95/final https://doi.org/10.6028/NIST.SP.800-95>
[^10]: Oldehoeft, A. E. (1992). Foundations of a security policy for use of the National Research and Educational Network. Gaithersburg, MD. NIST. <https://doi.org/10.6028/NIST.IR.4734>

```mermaid
graph TD;
    A[Policy Rules] --> |defines| B[general Gaia-X Policies]
    B --> |basis for| C[Resource-specific Policies]
    C -->D[Provider Policy/ Usage Policy]
    C -->E[Consumer Policy/ Search Policy]
```

## Federation Services

Federation Services are services suggested for the operational
implementation of an ecosystem. They are explained in greater
detail in the [Federation Services](federation_service.md) section.

They comprise four groups of services that are necessary to enable
Federation of Resources, Participants and interactions between
ecosystems. The four service groups are Identity and Trust, Federated
Catalogue, Sovereign Data Exchange and Compliance.

## Service Offerings, Service Instances and Service Contract

A Service Offering is defined as a set of Resources, which a Provider aggregates and publishes as a single entry in a Catalogue. Service Offerings may themselves be aggregated realizing Service Composition.  

A Service Instance is the instantiation of a Service Offering at runtime, strictly bound to a version of a Self-Description.

During the ordering phase, the Provider is invited to generated a denormalized version of a self-description for the newly instantiated Service Instance. The format and content of this self-description is out of scope for this document.

A Contract is an agreement between a Consumer and a Provider, to allow and regulate the usage of one or more Service Instances. It is related to a specific version of a Service, from which it derives the attributes of the Service Instances to be provisioned. The Contract has a distinct lifecycle from the Service Offering and additional attributes and logic, which are out of scope of Gaia-X Architecture Document.

## Contract

The Gaia-X Association is not getting involved into the realisation of the `Contract`. However, in order to ease participants with the establishement and to enter into a contractual relationship, we are defining below a common model for `Contract`.

### Concept: Computable Contracts as a service

- Contracts are the basis for business relationships.
- Whereas a licensor has rights with respect to a resource and is willing to (sub-)license such rights by a defined set of conditions.
- Whereas a licensee would like to get license rights with respect to a resource by a defined set of conditions.
- Licensor and licensee agree on it in form of a contract.
- Every role of the Gaia-X Conceptual Model as well as of operational model can be seen as legal persons and therefore may have a role as a licensor or licensee or both.
- In traditional centralized driven ecosystems the platform provider which is very often the ecosystem owner, defines the contractual framework and participants need to accept without any possibility for negotiation.
- In distributed and federated ecosystems individual contracting becomes much more important to support individual content of contractual relations, e.g., individual sets of conditions.
- The ability to negotiate on contracts is key for a sovereign participation. The ability to observe if all parties of a contract behave the way it is agreed, to validate their rights, to fulfill their obligations and ensure that no one can misuse information is key for a trustful relationship.
- Computable contracts aim to easy the complex processes of contract design, contract negotiation, contract signing, contract termination as well as to observe the fulfillment of contractual obligations and compliance with national law.

```mermaid
flowchart TB
participant[Legal Person]
op_role[Operational Model roles]
cm_role[Conceptual Model roles]
compliance[Gaia-X Compliance]
licensor[Licensor]
licensee[Licensee]
lic_rights[License rights]
usage[Gaia-X resource usage]
t_c[Terms & Conditions]
contract[Contract]
cc_elts[Computable Contract Elements]
cc[Computable Contract]
ncc[Non computable Contract]

op_role & cm_role -- defines --> participant

participant -- is a --> licensee & licensor

licensee -- requests --> lic_rights
licensor -- owns --> lic_rights

licensee -- accepts --> contract
licensor -- offers --> contract

lic_rights -- determines --> usage


lic_rights -- sub-licensed_by --> t_c

t_c -- defined by --> contract

contract -- represented by --> ncc & cc

cc -- consists of --> cc_elts

cc_elts -- verified by --> compliance
participant -- verified by --> compliance
```

## Additional Concepts

### Service Instances, Contracts, and Further Actors

In addition to those concepts and their relations mentioned above,  further ones exist in the Conceptual Model that are not directly governed by Gaia-X. These concepts do not need to undergo any procedures directly related to Gaia-X, e.g., do not create or maintain a Gaia-X Self-Description.

First, the **Service Instance** realizes a Service Offering and can be used by End-Users while relying on a contractual basis.

Second, **Contracts** are not in scope of Gaia-X but present the legal basis for the Services Instances and include specified Policies. Contract means the binding legal agreement describing a Service Instance and includes all rights and obligations.

Further relevant actors exist outside of the Gaia-X scope in terms of
**End-Users** and **Resource Owners**.

Resource Owners describe a natural or legal person, who holds the rights to Resources that will be provided according to Gaia-X regulations by a Provider and legally enable its provision. As Resources are bundled into a Service Offering and nested Resource compositions can be possible, there is no separate resource owner either. Resources can only be realized together in a Service Offering and Service Instance by a Provider, which presents no need to model a separate legal holder of ownership rights.

End-Users use service offerings of a Consumer that are enabled by Gaia-X. The End-User uses the Service Instances containing Self-Descriptions and Policies.

### Gaia-X and Data Meshes

**Data meshes** have emerged recently (since 2018) as an answer to the increasing difficulties of (logically) centralized and (conceptually) monolithic cloud data warehouses, data lakes, data lakehouses and other typically cloud-based approaches to the enterprise-wide management of analytical data to scale and to provide the required flexibility in today's highly dynamic and considerably complex business environments.

This section briefly outlines how Gaia-X incorporates several foundational principles of data meshes, where Gaia-X transcends the narrow boundaries of a data mesh approach, and why Gaia-X then may also be characterized as a **mesh of service meshes**.

#### Data Mesh Definition

The definition provided by the initial promotor of this concept, Zhamak Dehghani (Dehghani 2022), is widely accepted, accurate, and highly useful (Machado *et al.* 2022, BITKOM 2022):

	A data mesh is a decentralized socio-technical approach to share and manage analytical data in complex and large scale environments within or across organizations.

"Socio-technical approach" indicates that organizations need to adapt their processes and governance in addition to providing the right technology to successfully implement a data mesh. *Sharing*, for obvious reasons, also include all required capabilities to *access* the data. Originally, data meshes almost exclusively focused on **analytical data** used to support enterprise decision making as opposed to directly undergird operational and transactional data management happening in the various IT systems (e.g., ERP, CRM, PDM, MES,...). Recent trends, though, including concepts such as *data hubs* and *data fabrics*, try to overcome this limitation towards a more holistic approach to enterprise data management.

Data meshes closely observe and implement the following four principles:

Principle | Explanation
--------- | -----------
**domain ownership** | (Analytical) data shall be *owned* by different cross-functional (as oposed to specialized!) teams organized around and within appropriate business domains similar to domain-driven design (Evans 2004).
**data as a product** | (Analytical) data shall be managed and treated like a true product which is *produced* by someone in order to provide a well-defined *value* to its *consumers*.
**self-serve data platform** | to enable the different domain teams by providing domain-agnostic capabilities often needed in a *data as a product* approach such as, for instance, data product life cycle management, pipeline workflow management, data processing frameworks, policy enforcement commponents, and many others.
**federated computational governance** | Data meshes need an appropriate data governance operating model to balance the autonomy and agility of the individual domains with the global interoperability of the overall data mesh (Dehghani 2022, 8). Typically, this will be accomplished by a cross-functional team composed of domain experts, data platform specialists, and other suitable subject matter experts (legal, security, compliance) complemented by heavy automation (e.g., *policies as code*, test and monitoring automation)

#### Gaia-X as (Higher Order) "Service Mesh"

Gaia-X itself incorporates several principles present in a data mesh with the marked (and defining) difference that Gaia-X relies on a **service-oriented apporach** at the center of its conceptual model as opposed to restricting itself to analytical data. This generalization notwithstanding, Gaia-X co-opts several usability characteristics of *data as a product* from the data mesh approach such as

- discoverability and addressability of services (e.g., Federated Catalog, self-sovereign identities)
- providing trustworthy and truthful services through its Trust Framework
- interoperability and composability of services
- guaranteeing secure consumption of services through automated policies (*policies as code*), monitoring, and the Gaia-X Data Exchange Services

Gaia-X Federation Services and the Trust Framework provide capabilities similar to the *self service data platform* of a data mesh. Also corresponding to data mesh philosophy, there exists a dedicated domain-independent organizational unit to operate and manage this platform as well, the Federator. Yet, in contrast to the smaller focus of data meshes, the Gaia-X Ecosystem is designed *ex ante* for federating independent autonomous ecosystems itself (cf. the three Gaia-X *planes* in section Gaia-X ecosystems).

Like with data meshes, the organizational structure of Gaia-X itself (and also Gaia-X based ecosystems) also rely on a substantially federated governance model when considering the Gaia-X Association (or Gaia-X ecosystem federators), its Committees, Working Groups, and decision processes. In the case of Gaia-X (or Gaia-X based ecosystems), though, all actors in the various governance functions belong to different legal entities unlike typical data mesh situations where they appertain to a single organization or enterprise.

The term "domain" in data mesh implementations characteristically denotes "bounded contexts" such as spheres of knowledge, influence, activities, or responsibilities *within* a potentially large single organization (Evans 2004, Deghgani 2022). At face value, its Gaia-X equivalent, `Participant`, possesses almost identical semantics: "A `Participant` is an entity as defined in ISO/IEC 24760-1 as an "item relevant for the purpose of operation of a domain that has recognizably distinct existence". Practically, though, Gaia-X `Participants` will mostly consist of independent legal entities.

In summary, a high degree of similarity and overlap between the principles applied for data meshes and for Gaia-X can be recognized. On the other hand, the deviations of Gaia-X with respect to the data mesh approach identified above amount to a form of conceptual generalization. Whereas data meshes primarily concentrate on the management of (analytical and other) data for a single (albeit large and complex) organization, typically a legal entity, Gaia-X provides organizational standards and technical components for realizing whole ecosystems consisting of several independent organizations. Extending this line of thinking one may be even (rightfully) enticed to designate Gaia-X as a **mesh of service meshes** (not just a mesh of data meshes). Note, however, that this particular usage of the term "service mesh" is different from the one encountered in microservice architectures[^service-mesh-classical].

[^service-mesh-classical]: In (typically container-based) microservices architectures a service mesh provides software components and mechanisms to separate cross-cutting concerns of service-to-service communications from the business logic of the individual microservices into a so-called “control plane”. This segregation simplifies development, operations, and management of larger microservices environments. It is achieved by making every microservice communicate with another one (the so-called east/west traffic) over dedicated components called proxies. See, for instance, [Tech Radar - Service Mesh](https://techradar.softwareag.com/technology/service-meshes/).

## Examples

### Personal Finance Management example

This example describes the various Gaia-X concepts using the Open Banking scenario of a Personal Finance Management service (PFM) in SaaS mode.

Suppose that the service is proposed by a company called `MyPFM` to an end user `Jane` who has bank accounts in two banks: Bank<sub>1</sub> and Bank<sub>2</sub>.  
`MyPFM` is using services provided by Bank<sub>1</sub> and Bank<sub>2</sub> to get the banking transactions of `Jane` and then aggregates these bank statements to create Jane’s financial dashboard.

`Jane` is the **End-User**.

Bank<sub>1</sub> and Bank<sub>2</sub> are **Providers** defining the **Service Offerings** delivering the banking transactions and operating the corresponding **Service Instances**. They are also **Resource Owners** for the bank statements, which are **Resources** composing the **Service Offerings** (`Jane` is the data subject as per GDPR[^gdprdatasubject]).  
The associated **Resource Policies** are in fact predefined by the PSD2[^PSD2] directive from the European Parliament.

[^gdprdatasubject]: Rights of the data subject <https://gdpr-info.eu/chapter-3/>
[^PSD2]: Payment services (PSD 2) <https://ec.europa.eu/info/law/payment-services-psd-2-directive-eu-2015-2366_en>

`MyPFM` is the **Consumer** which consumes the **Service Instances** provided by Bank<sub>1</sub> and Bank<sub>2</sub> in order to create a financial dashboard and to offer it to `Jane`.  
`MyPFM` is also likely consuming **Service Instances** from a PaaS **Provider** in order to run its own code, such as dashboard creation.

### Service Composition Annex 1

#### Practical multi-cloud/-provider/-service composition example

Figure 5 presents a pragmatic and practical multi-cloud, multi-provider and multi-service example (or use case) that reflects the current state of the art and practice in cloud services and computing.

![Multi Services Multi Provider Scenario](figures/Fig5_SC_conceptual_model-Multiservices-Multiprovider_Draft-by-ME+DZ.png)
*Figure 5. Practical, service template oriented, multi cloud/provider/service composition example*

This example provides a more concrete example of what can be readily achieved today with available cloud services “deployment and management tools” and cloud services providers. The example is in line with the generic service composition model of Figure 3, differences are more in how the class diagram is organized and configured, but all key functions and blocks are identical and common.

The example illustrates how multiple cloud providers are selected to fulfil a user service request for a distributed application across multiple hosting infrastructures and providers. The requested service in this case requires an independent interconnection service provider to ensure connectivity across multiple clouds. The service composition is service-template oriented and relies on selection and configuration of service templates to implement a solution compliant with end users and Gaia-X. From the generic implementation plan result of the output of the Services Composition Tool, the suppliers (or the end user) using an orchestrator will adapt the plan to the solutions of the different suppliers. This is achieved through adapted templates (usually in implementation models such as Terraform, Ansible, Heat or others) made available from the pool of available resources.
