# Gaia-X Operating Model

Gaia-X in its unique endeavour must have an operating model enabling a widespread adoption by small and medium-sized enterprises up to large organisations, including those in highly regulated markets, to be sustainable and scalable.

To achieve the objectives above, a non-exhaustive list of Critical Success Factors (CSFs) includes these points:

1. The operating model must provide clear and unambiguous added value to all Participants
2. The operating model must have a transparent governance and trust model with identified accountability and liability, that is clearly and fully explained to all Participants
3. The operating model must be easy to use by all Participants
4. The operating model must be financially sustainable for the Gaia-X Ecosystem
5. The operating model must be environmentally sustainable.

The first part of this chapter introduces the Gaia-X Ecosystem, as well as Trust Anchors.  Trust Anchors are defined, including details about who defines them and how they will be nominated.  

The second part defines Gaia-X Compliance, and how to become compliant. It introduces the Gaia-X Compliance Service as well as the usage of Gaia-X Labels.

Finally, the last section will cover the Gaia-X Self-Descriptions life-cycle and the Gaia-X Registry, which provides essential support for the Gaia-X Decentralized Autonomous Ecosystem.

## Gaia-X Ecosystem

An ecosystem is an independent group of Participants that directly or indirectly consume, produce, or provide services such as data, storage, computing, network services, including combinations of them.

Those Participants autonomously decide which information to share, with whom, and if they want to do it in a Gaia-X compliant way.

Independently of the functional, business scope or interoperability levels of those ecosystems, the Gaia-X Ecosystem is the virtual set of all entities described with a Gaia-X compliant Self-Description and that conform to Gaia-X requirements.

Several individual Ecosystems may exist that orchestrate themselves, use the Architecture and may or may not use the Federation Services open source software.

Examples of Ecosystems:

- [Catena-X](https://catena-x.net/) - Automotive ecosystem
- [Agdatahub](https://agdatahub.eu/en/) - Agriculture ecosystem

Access to an ecosystem is under the full control of the participants providing the ecosystem's federation services, the ecosystem's federators.

Only the Self-Descriptions following the requirement of the Gaia-X Trust Framework are eligible for Gaia-X compliance.

Only compliant Service Offering Self-Descriptions are eligible to Gaia-X Labels.

More details are given in the [Gaia-X ecosystem](ecosystem.md) chapter.

## Trust Anchors

For a given ecosystem, the Trust anchors are the entities considered by all Participants to be trustworthy when establishing the chain of cryptographic certificates.  
Ecosystems can select their own Trust Anchors, however, cross-ecosystem trust requires the selected Trust Anchors to comply at least with the same rules that the common Gaia-X ecosystem Trust Anchors shall comply with.

The Gaia-X Association defines:

- the sets of rules to define the Trust Anchors:
    - [Trust Service Providers](https://en.wikipedia.org/wiki/Trust_service_provider).
    - Gaia-X Label Issuers
    - Trusted data source for Gaia-X Compliance
- the format of the Self-Descriptions and their compliance rules
- the [Gaia-X Policy Rules and Labelling Document](https://docs.gaia-x.eu).

```mermaid
flowchart TB
    gx((Gaia-X<br>Association))
    part((Participants))
    label[Gaia-X Labels]
    sd[Self-Description Files]

    subgraph ta[Trust Anchors]
    end
    gx -- defines compliance rules for --> ta & sd

    gx -- defines --> label
    part -- nominate --> ta
```

The Trust Anchors are nominated by the Participants. The validation of the nominees is done automatically by validating the rules defined by the Gaia-X Association and supervised by the Gaia-X Association.

In turn, the Trust Anchors are used by the Participants to operate the Ecosystem(s).

```mermaid
flowchart TB
    subgraph ta[Trust Anchors]
        cab{{Gaia-X Label Issuers}}
        tsp{{Trust Service Providers}}
        tds{{Trusted Data Sources<br>for Gaia-X compliance}}
    end

    cab -- issues --> label[Gaia-X Labels]
    tsp -- proves --> id["Principal#commat;Participant's identity"]
    tds -- validates --> attr[Self-Description's attribute]
```

## Gaia-X Trust Framework

The Gaia-X Trust Framework is defined as the process of going through and validating the set of automatically enforceable rules to achieve the minimum level of Self-Description compatibility in terms of:

- syntactic correctness.
- schema validity.
- cryptographic signature validation.
- attribute value consistency.
- attribute value verification.

Whenever possible, the verification of Self-Descriptions' attribute values is done either by using publicly available open data, and performing tests or using data from Trusted Data Sources as defined in the previous section.
This verification is captured using Verifiable Credentials issued by either:
- the Gaia-X association when performing live tests (Trust Anchor)
- the owner of the Trusted Data source (Trust Anchor)


However, it is expected that checking the validity of Self-Descriptions using data will introduce costs. In the context of the Gaia-X Ecosystem, a proposal to cover the operating cost is described later in this document with the introduction of a [Gaia-X Decentralized Autonomous Ecosystem](#gaia-x-decentralized-autonomous-ecosystem).

:information_source: Other ecosystems are autonomous and this operating model does not cover how the operating cost of ecosystems should be handled.

The set of rules is versioned and will evolve over time to adapt to legal and market requirements. Those rules are set in a separate Gaia-X Trust Framework document.[^TFdraft]

[^TFdraft]: Gaia-X Trust Framework draft: https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/

The rules will be implemented using open-source code and a service instance of that source code is called a Gaia-X Compliance Service.

**One of the first Gaia-X added values is the creation of a [FAIR](https://www.go-fair.org/fair-principles/) (findable, accessible,interoperable, reusable) knowledge graph of verifiable and composable Self-Descriptions.**  

## Gaia-X Labels

From the [European Data Governance Act](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:52020PC0767) proposal:

> As a compulsory scheme this could generate higher costs, which could potentially have a prohibitive impact on SMEs and startups, and the market is not mature enough for a compulsory certification scheme; therefore, lower intensity regulatory intervention was identified as the preferred policy option.  
> However, the higher intensity regulatory intervention in the form of a compulsory scheme was also identified as a feasible alternative, as it would bring significantly higher trust to the functioning of data intermediaries, and would establish clear rules for how these intermediaries are supposed to act in the European data market.

The decision for the Gaia-X Association is to adopt a compulsory scheme for Gaia-X compliance – see previous section – and an optional scheme for Gaia-X Labels, to ensure a common level of transparency and interoperability while limiting the regulatory burden on the market players.

Labels are issued for Service Offerings only and are the result of the combination of several Self-Description compliant attributes, that individually would be insufficient to support business or regulatory decisions.  
The issued Labels must include a version number to allow continuous evolution of the set of rules and the precise set of rules in a "rulebook" defined by the Gaia-X Association, which must include a workflow for compliance re-validation.

From a technical point of view, a Label is a [W3C Verifiable Credential](https://www.w3.org/TR/vc-data-model/#proofs-signatures), similar to Self-descriptions' attributes credentials that are described in the next section.

The management of the rulebook and its governance is described in the "Gaia-X Labelling Framework" document published on the [Gaia-X Associations Web site] https://gaia-x.eu/mediatech/publications/ .

## Gaia-X Self-Description

Gaia-X Self-Descriptions describe in a machine interpretable format any of the entities of the Gaia-X Conceptual Model.

```mermaid
flowchart LR
    entity[Entity]
    sd[Self-Descriptions]
    schema[Schema]
    entity -- described by --> sd
    sd -- validated against --> schema
```

It means that there are Self-Descriptions for Participants in all roles: `Consumer`, `Federator`, `Provider` and all the other entities in an Ecosystem's scope such as `Resource` and `Service Offering`.

Each Gaia-X entity makes [Claim](https://www.w3.org/TR/vc-data-model/#claims)s, which are validated and signed by 3rd parties. Those signed Claims are defined as [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials) and presented by the entity as [Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations).  

Technically speaking, Self-Descriptions are [W3C Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations) in the [JSON-LD serialization of the RDF graph data model](https://www.w3.org/TR/json-ld11/).

The following workflow describes how Gaia-X Self-Descriptions are created following the vocabulary of the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/#ecosystem-overview) standard.


| W3C Term                                                                      | Example with a car                                                          |
|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| [Claim](https://www.w3.org/TR/vc-data-model/#claims)                          | My car is red                                                               |
| [Verifiable Credential](https://www.w3.org/TR/vc-data-model/#credentials)     | The garage's attestation that my car is red                             |
| [Verifiable Presentation](https://www.w3.org/TR/vc-data-model/#presentations) | Me showing to my friend the garage's attestation that my car is red |
| Issuer                                                                        | The garage                                                                  |
| Holder                                                                        | Myself                                                                      |
| Verifier                                                                      | My friends                                                                  |

```mermaid
sequenceDiagram
    autonumber
    participant part as Participant - Holder
    participant val as Participant - Issuer<br>or<br>Gaia-X Label Issuer

    activate part
        part ->> part: creates Claims and self-signs them
        loop for each Claim
        opt when possible
            part ->> val: requests verification.
            Note over val: performs automated and/or manual checks.
            val ->> part : issues Verifiable Credential (VC) as proof.
        end
        end
    deactivate part
    part ->> part: compiles all VCs into a Verifiable Presentation (VP).
```

### Difference between Self-Description's proofs (VC), Gaia-X Trust Framework and Gaia-X Labels.

The Verifiable Credentials are issued by other Participants, including Conformity Assessment Bodies. Verifiable Credentials can also be used to build a reputation system in the knowledge graph.

The Gaia-X Compliance ensures that the required level of information for the users to take educated decisions is available and the information is verified or verifiable.

The Gaia-X Labels guarantee that Service Offerings meet a specific set of respective requirements with regards to the main objectives: data protection, transparency, security, portability and flexibility, and European control. 

|                          | Attribute's Verifiable Credentials | Gaia-X Trust Framework          | Gaia-X Labels              |
|--------------------------|------------------------------------|----------------------------|----------------------------|
| Technical implementation | W3C Verifiable Credentials         | W3C Verifiable Credentials | W3C Verifiable Credentials |
| Credential Issuer        | Any Gaia-X Participant             | Gaia-X Compliance Service  | Gaia-X Label Issuer        |
| Application scope        | All Self-Descriptions              | All Self-Descriptions      | Service Offerings          |
| Assessment method(s)     | Manual or Automated                | Fully automated            | Manual or Automated        |
| Issuance's temporality   | Frequent updates                   | Frequent updates           | Slow updates (~yearly)     |

A attribute's Verifiable Credential is Gaia-X conformant if the Issuer of the Verifiable Credential has itself an identity coming from one of the Trust Anchors.

```mermaid
flowchart LR
    part["Participants"]

    subgraph ta[Trust Anchors]
        tsp{{Trust Service Provider}}
    end

    subgraph sd[Self-Descriptions]
        cred[Credentials]
    end

    tsp -- proves identity of --> part

    part -- hold --> sd
    part -- issue --> cred
    part -- verify --> cred
```

A Label is Gaia-X conformant if the Issuer of the Credential is one of the Trust Anchors' Gaia-X Label Issuers.

```mermaid
flowchart LR
    part["Participants"]

    subgraph ta[Trust Anchors]
        cab{{Gaia-X Label Issuers}}
    end

    subgraph sd[Self-Descriptions]
        cred["Gaia-X Labels<br>&#40;Credentials&#41;"]
    end

    part -- hold --> sd
    cab -- issue --> cred
    part -- verify --> cred
```

### Self-Description compliance

A Self-Description qualified as Gaia-X compliant must be submitted to a Gaia-X Compliance Service instance as defined in the section above.

The result of that submission is captured in two ways:

- as a Verifiable Credential: If the compliance is validated, the Gaia-X Compliance Service issues a Verifiable Credential that can later be inserted into the Self-Description. This method aligns with the self sovereign principle of the holder being in charge of the information.
- If the Gaia-X Compliance Service is called via the Gaia-X Registry, the Gaia-X Registry will emit an event to synchronize Catalogues. The event contains the URL the of the Self-Description file. The Gaia-X Registry is defined in the next section.

```mermaid
sequenceDiagram
    autonumber
    participant part as Participant
    participant comply as Gaia-X Compliance Service
    participant registry as Gaia-X Registry
   
    Note over part: decides where to store the Self-Description files.

    part ->> comply: call service to validate Self-Description.
    activate comply
rect rgba(255, 128, 0, .1)
    comply -) comply: performs Gaia-X compliance check.
end
    comply ->> part: issue a Verifiable Credential
    opt if called via the Gaia-X Registry
    comply -) registry: emit events with URLs<br>of validated Self-Description.
    end
    deactivate comply
```

### Self-Description Remediation

Self-Descriptions may become invalid over time. There are three states declaring a Self-Description as invalid:

- Expired (after a timeout date, e.g., the expiry of a cryptographic signature)
- Deprecated (replaced by a newer Self-Description)
- Revoked (by the original issuer or a trusted party, e.g., because it contained incorrect or fraudulent information)

Expired and Deprecated can be deduced automatically based in the information already stored in the Gaia-X Registry or Gaia-X Catalogues. There are no additional processes to define. This section describes how Self-Descriptions are revoked. 

The importance of Gaia-X compliance will grow over time, covering more and more Gaia-X principles such as interoperability, portability, and security. However, automation alone is not enough and the operating model must include a mechanism to demotivate malicious actors to corrupt the Registry and Catalogues.

The revocation of Self-Descriptions can be done in various ways:

- **Revocation or Deprecation by authorship**: The author of a Self-Description revokes or deprecates the Self-Description explicitly.
- **Revocation by automation**: The Gaia-X Compliance Service found at least one self-described attribute not validating the compliance rules.
- **Suspension and Revocation by manual decision**: After an audit by a compliant Gaia-X Participant, if at least one self-described attribute is found to be incorrect, the suspension of the Self-Descriptions is automatic. The revocation is submitted for approval to the Gaia-X Association with the opportunity for the Self-Description's owner to state its views in a matter of days. To minimize subjective decisions and promote transparency, the voting results will be visible and stored on the Gaia-X Registry or in the local Ecosystem's Registry.
<!-- - Provider found to be convicted by the Court of Justice of the European Union - CJEU - or a European's Member State court of justice on Data Breach will lead to the revocation of all Provider's Services Offerings's Labels. -->

## Gaia-X Decentralized Autonomous Ecosystem

The operating model described in this chapter motivates the creation of a Gaia-X decentralized autonomous Ecosystem following the principles of a Decentralized Autonomous Organisation[^dao], with the following characteristics:

- Compliance is achieved through a set of automatically enforceable rules whose goal is to incentivize its community members to achieve a shared common mission.
- Maximizing the decentralization at all levels to reduce lock-in and lock-out effects.
- Minimizing the governance and central leadership to minimize liability exposure and regulatory capture.
- The ecosystem has its own rules, including management of its own funds.
- The ecosystem is operated by the ecosystem's Participants

[^dao]: Example of the setup of a DAO <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

:information_source: Other ecosystems are autonomous and this operating model does not enforce how internal ecosystem governance should be handled.

### Gaia-X Registry

The Gaia-X Registry is a public distributed, non-repudiable, immutable, permissionless database with a decentralized infrastructure and the capacity to automate code execution.

:information_source: The Ecosystems may want to have their own instance of a local Registry or equivalent. Technically, this component can be part of the ecosystem local Catalogues.

The Gaia-X Registry is the backbone of the ecosystem governance which stores information, similarly to the [Official Journal of the European Union](https://eur-lex.europa.eu/oj/direct-access.html), such as:

- the list of the Trust Anchors – keyring.
- the result of the Trust Anchors validation processes.
- the potential revocation of Trust Anchors identity.
- the vote and results of the Gaia-X Association roll call vote, similar to the rules of the [plenary of the European Parliament](https://www.europarl.europa.eu/about-parliament/en/organisation-and-rules/how-plenary-works)
- the URLs of the Self-Description Schemas defined by Gaia-X
- the URLs of Catalogue's Self-Descriptions
- … 

It also facilitates the provision of:

1. A decentralized network with smart contract functionality.
2. Voting mechanisms that ensure integrity, non-repudiation and confidentiality.
3. Access to a Gaia-X Compliance Service instance.
4. A fully operational, decentralized and easily searchable catalogue[^OP].
5. A list of Participants' identities and Self-Description URIs which violate Gaia-X membership rules. This list must be used by all Gaia-X Trusted Catalogue providers to filter out any inappropriate content.
6. Tokens may cover the operating cost of the Gaia-X Ecosystem. This specific point can be abstracted by 3rd party brokers wrapping token usage with fiat currency, providing opportunities for new services to be created by the Participants. Emitting tokens for the Gaia-X Association's members is also considered.

:information_source: Each entry in the Gaia-X Registry is considered as a transaction. A transaction contains [DID](https://w3c.github.io/did-core/)s of all actors involved in the transaction and metadata about the transaction in a machine readable format.  The basic rule for a transaction to be valid is that all DIDs have one of the Trust Anchors as root Certificate Authorities.  Please also note that the Registry stores all revoked Trust Anchors.

This model enables the Participants to operate in the Gaia-X Ecosystem, to autonomously register information, and to access the information which is verifiable by other Participants.  

[^OP]: Example of decentralized data and algorithms marketplace <https://oceanprotocol.com/>
### Ecosystem launching phase

In order to enable the 1st scenario which is:

> As a Gaia-X Provider, I want to publish the Self-Description of my Service Offerings and I want my Service Offerings to be made available to all ecosystems.

and until the inter-catalogue synchronization is documented, the Registry will also be used to store, directly or indirectly via an external storage, the Self-Descriptions' URLs.

### Verifiable Presentation Verification

The Gaia-X Registry, or a private one, independently of its implementation, is the single source of truth for the Ecosystem.  
It allows any `Participant` to verify the validity of signatures.

```mermaid
sequenceDiagram
    autonumber
    participant cat as Catalogue
    participant user as Consumer
    participant registry as Gaia-X Registry
    user ->> cat: searches for services<br>by requirements.
    cat ->> user: returns results.
rect rgba(128, 128, 0, .1)
    Note left of  registry: Those checks can also be done by the<br>catalogues to provide a more user-friendly<br>experience for non expert users.
    loop for each Provider
        user ->> registry: can verify if the Provider's identity is from<br>a Gaia-X compliant Trust Anchor.
    end

    loop for each Claim
        user ->> registry: can verify if the VC Issuer's identity is from<br>a Gaia-X compliant Trust Anchor.
    end

    loop for each Label
        user ->> registry: can verify if the Labels are from<br>a Gaia-X compliant Trust Anchor.
    end

    loop for each signature
        user ->> registry: can verify if any signatures were revoked.
    end
end
```
*Example with `Consumer` and main `Gaia-X Registry`*

<!-- ### Data curation

By offering transparent access to the Gaia-X registry structured and verifiable Service-Descriptions, plus visibility on Service Instance consumption, the Participants can extrapolate about the data quality.

Other metadata, such as using [Great Expectations](https://github.com/great-expectations/great_expectations) can be enforced at the Data interoperability layer to promote a Data quality score. -->
